<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Warung extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Warung_model', 'WarungMenu_model', 'WarungReview_model'));
    }

    function response($response) {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    function row($field, $value) {
        $where = array(
            $field=>$value
        );
        $count = $this->Warung_model->count($where);
        if($count > 0) {
            $success = TRUE;
            $message = 'Data Berhasil Didapatkan';
            $data = $this->Warung_model->row($where);
        } else {
            $success = FALSE;
            $message = 'Data Tidak Ditemukan';
            $data = [];
        }

        $this->response([
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        ]);

        echo '123';
    }

    function all() {
        $success = TRUE;
        $message = 'Data Berhasil Didapatkan';
        $data = $data = $this->Warung_model->all();

        $this->response([
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        ]);
    }

    function check_valid_json($val) {
        json_decode($val);
        if(json_last_error() == JSON_ERROR_NONE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_valid_json', 'Kolom tidak terisi dengan format JSON');
            return FALSE;
        }
    }

    function check_valid_time($val) {
        $check = strtotime($val);
        if ($check == '') {
            $this->form_validation->set_message('check_valid_time', 'Kolom tidak terisi dengan format Time');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function filter() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('priceMin', '', 'trim|required|integer');
        $this->form_validation->set_rules('priceMax', '', 'trim|required|integer');
        $this->form_validation->set_rules('operationDay', '', 'trim|required|callback_check_valid_json');
        $this->form_validation->set_rules('openTime', '', 'trim|required|callback_check_valid_time');
        $this->form_validation->set_rules('closeTime', '', 'trim|required|callback_check_valid_time');
        $this->form_validation->set_rules('keywords', '', 'trim');
        $this->form_validation->set_error_delimiters('','');

        if($this->form_validation->run() === FALSE) {
            $success = FALSE;
            $message = 'Mohon form dilengkapi dengan benar';
            $data = array(
                'form'=>array(
                    'priceMin'=>form_error('priceMin'),
                    'priceMax'=>form_error('priceMax'),
                    'operationDay'=>form_error('operationDay'),
                    'openTime'=>form_error('openTime'),
                    'closeTime'=>form_error('closeTime'),
                )
            );

        } else {
            $priceMin = $this->input->post('priceMin');
            $priceMax = $this->input->post('priceMax');
            $operationDay = json_decode($this->input->post('operationDay'), TRUE);
            $openTime = $this->input->post('openTime');
            $closeTime = $this->input->post('closeTime');
            $keywords = $this->input->post('keywords');

            $success = TRUE;
            $message = 'Data Berhasil Didapatkan';
            $data = $this->WarungMenu_model->warungFilter($keywords, $priceMin, $priceMax, $operationDay, $openTime, $closeTime);
        }

        $response = array(
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        );

        $this->response($response);
    }

    function row_menu($field, $value) {
        $where = array(
            $field=>$value
        );
        $count = $this->Warung_model->count($where);
        if($count > 0) {
            $where_menu = array(
                'warungId'=>$id
            );
            $success = TRUE;
            $message = 'Data Berhasil Didapatkan';
            $data = $this->WarungMenu_model->warungMenu($where_menu);
        } else {
            $success = FALSE;
            $message = 'Data Tidak Ditemukan';
            $data = [];
        }

        $response = array(
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        );

        $this->response($response);
    }

    function row_review($id) {
        $where = array(
            'id'=>$id
        );
        $count = $this->Warung_model->count($where);
        if($count > 0) {
            $where_review = array(
                'warungId'=>$id
            );
            $success = TRUE;
            $message = 'Data Berhasil Didapatkan';
            $data = $this->WarungReview_model->warungReview($where_review);
        } else {
            $success = FALSE;
            $message = 'Data Tidak Ditemukan';
            $data = [];
        }

        $response = array(
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        );

        $this->response($response);
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// test keyboard

class User extends CI_Controller {

    function __construct()
    {
        
        parent::__construct();
        $this->load->model(array('User_model'));
    }

    function response($response) {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    function check_user_exist($id) {
        $where = array(
            'id'=>$id
        );
        $check = $this->User_model->num_rows($where);
        if($check <= 0) {
            $success = FALSE;
            $message = 'User tidak ditemukan yoaaa';
            $data = array();

            $this->response(array(
                'success'=>$success,
                'message'=>$message,
                'data'=>$data
            ));
        }
    }

    function profile($id='') {
        $this->check_user_exist($id);
        $success = TRUE;
        $message = 'Data Berhasil Didapatkan';
        $data = $this->User_model->row(array('id'=>$id));

        $this->response([
            'success'=>$success,
            'message'=>$message,
            'data'=>$data
        ]);
    }

    function check_user_email_exist($val) {
        $where = array(
            'email'=>$val
        );
        $check = $this->User_model->num_rows($where);
        if($check > 0) {
            $this->form_validation->set_message('check_user_email_exist', 'Email tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function register() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', '', 'trim|required');
        $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_check_user_email_exist');
        $this->form_validation->set_rules('phone', '', 'trim|required');
        $this->form_validation->set_rules('password', '', 'trim|required');
        $this->form_validation->set_rules('thumbnail', '', 'trim');
        $this->form_validation->set_rules('facebookId', '', 'trim');
        $this->form_validation->set_rules('googleId', '', 'trim');
        $this->form_validation->set_error_delimiters('','');

        if($this->form_validation->run() === FALSE) {
            $success = FALSE;
            $message = 'Kolom belum benar terisi';
            $data = array(
                'form'=>array(
                    'name'=>form_error('name'),
                    'email'=>form_error('email'),
                    'phone'=>form_error('phone'),
                    'password'=>form_error('password'),
                    'thumbnail'=>form_error('thumbnail'),
                    'facebookId'=>form_error('facebookId'),
                    'googleId'=>form_error('googleId')
                )
            );
            $response = array(
                'success'=>$success,
                'message'=>$message,
                'data'=>$data
            );
            $this->response($response);
        } else {
            $data = array(
                'name'=>$this->input->post('name'),
                'email'=>$this->input->post('email'),
                'phone'=>$this->input->post('phone'),
                'password'=>$this->input->post('password'),
                'thumbnail'=>$this->input->post('thumbnail'),
                'facebookId'=>$this->input->post('facebookId'),
                'googleId'=>$this->input->post('googleId'),
                'createdAt'=>date('Y-m-d H:i:s')
            );
            $this->User_model->insert($data);

            $this->response(array(
                'success'=>TRUE,
                'message'=>'Berhasil menambahkan data',
                'data'=>array()
            ));
        }
    }

    function profile_update($id='') {

        $this->check_user_exist($id);
        $user = $this->User_model->row(array('id'=>$id));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', '', 'trim|required');

        if($user->email == $this->input->post('email')) {
            $this->form_validation->set_rules('email', '', 'trim|required|valid_email');
        } else {
            $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_check_user_email_exist');
        }

        $this->form_validation->set_rules('phone', '', 'trim|required');
        $this->form_validation->set_rules('password', '', 'trim|required');
        $this->form_validation->set_rules('thumbnail', '', 'trim');
        $this->form_validation->set_rules('facebookId', '', 'trim');
        $this->form_validation->set_rules('googleId', '', 'trim');
        $this->form_validation->set_error_delimiters('','');

        if($this->form_validation->run() === FALSE) {
            $success = FALSE;
            $message = 'Kolom belum benar terisi';
            $data = array(
                'form'=>array(
                    'name'=>form_error('name'),
                    'email'=>form_error('email'),
                    'phone'=>form_error('phone'),
                    'password'=>form_error('password'),
                    'thumbnail'=>form_error('thumbnail'),
                    'facebookId'=>form_error('facebookId'),
                    'googleId'=>form_error('googleId')
                )
            );
            $response = array(
                'success'=>$success,
                'message'=>$message,
                'data'=>$data
            );
            $this->response($response);
        } else {
            $data = array(
                'name'=>$this->input->post('name'),
                'email'=>$this->input->post('email'),
                'phone'=>$this->input->post('phone'),
                'password'=>$this->input->post('password'),
                'thumbnail'=>$this->input->post('thumbnail'),
                'facebookId'=>$this->input->post('facebookId'),
                'googleId'=>$this->input->post('googleId'),
                'createdAt'=>date('Y-m-d H:i:s')
            );
            $this->User_model->insert($data);

            $this->response(array(
                'success'=>TRUE,
                'message'=>'Berhasil memperbaharui profil',
                'data'=>array()
            ));
        }
    }
}

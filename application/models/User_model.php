<?php if(!defined('BASEPATH')) exit('No script access allowed');

class User_model extends CI_Model
{

    private $table = 'user';
    private $id = 'id';

    function __construct()
    {
        parent:: __construct();
    }

    function num_rows($where) {
        return $this->db->where($where)->get($this->table)->num_rows();
    }

    function row($where) {
        return $this->db->where($where)->get($this->table)->row();
    }

    function insert($data) {
        $this->db->insert($this->table, $data);
    }

}
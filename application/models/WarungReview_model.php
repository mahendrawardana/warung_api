<?php if(!defined('BASEPATH')) exit('No script access allowed');

class WarungReview_model extends CI_Model
{

    private $table = 'warungreview';
    private $table_warung = 'warung';
    private $id = 'id';

    function __construct()
    {
        parent:: __construct();
    }

    function warungReview($where) {
        return $this->db->select('wrv.*, usr.name as user_name')
                        ->join('user usr', 'usr.id = wrv.userId','left')
                        ->where($where)
                        ->order_by('name', 'ASC')
                        ->get($this->table.' wrv')
                        ->result();
    }
}
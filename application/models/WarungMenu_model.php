<?php if(!defined('BASEPATH')) exit('No script access allowed');

class WarungMenu_model extends CI_Model
{

    private $table = 'warungmenu';
    private $table_warung = 'warung';
    private $id = 'id';
    private $pathThumbWarung = 'images/warung/';
    private $urlThumbWarung;

    function __construct()
    {
        parent:: __construct();
        $this->urlThumbWarung = base_url().$this->pathThumbWarung;
    }

    function warungMenu($where) {
        return $this->db->where($where)
                        ->order_by('name', 'ASC')
                        ->get($this->table)
                        ->result();
    }

    function warungFilter($keywords, $price_min, $price_max, $operationDay, $openTime, $closeTime) {
        $data = $this->db->select('wr.*')
                         ->join($this->table_warung.' wr', 'wr.id = wm.warungId','left')
                         ->where(array(
                             'wm.price >='=> $price_min,
                             'wm.price <='=> $price_max,
                             'wr.openTime >='=>$openTime,
                             'wr.closeTime <='=>$closeTime,
                         ));
        if($keywords != '') {
            $data = $data->like('wr.name', $keywords)
                         ->or_like('wm.name', $keywords);
        }
        $data = $data->get($this->table.' wm')->result();

        $return = array();
        foreach($data as $key=>$r) {
            $operationDayTb = json_decode($r->operationDay, TRUE);
            foreach($operationDay as $day) {
                if(in_array($day, $operationDayTb)) {
                    $return[$key] = $r;
                    $return[$key]->thumbnail = $this->urlThumbWarung.$r->thumbnail;
                    break;
                }
            }
        }


        return $return;
    }
}
<?php if(!defined('BASEPATH')) exit('No script access allowed');

class Warung_model extends CI_Model
{
    private $table = 'warung';
    private $id = 'id';
    private $pathThumbWarung = 'images/warung/';
    private $urlThumbWarung;


    function __construct()
    {
        parent:: __construct();
        $this->urlThumbWarung = base_url().$this->pathThumbWarung;

    }

    function all()
    {
        $warung = $this->db->order_by($this->id, 'DESC')->get($this->table)->result();
        $data = array();
        foreach($warung as $key=>$r) {
            $data[$key] = $r;
            $data[$key]->thumbnail = $this->urlThumbWarung.$r->thumbnail;
        }

        return $data;
    }

    function row($where=array()) {
        $warung = $this->db->where($where)->get($this->table)->row();
        $warung->thumbnail = $this->urlThumbWarung.$warung->thumbnail;

        return $warung;
    }

    function count($where)
    {
        return $this->db->where($where)->get($this->table)->num_rows();
    }

}